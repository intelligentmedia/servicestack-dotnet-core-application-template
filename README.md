Steps to prepare ServiceStack .Net Core Template solution for a new application


Open solution

Rename the solution 

Rename ServiceStack.Template projects

Close solution

Rename project folders to match project names 

Edit sln file and change folder references to match the folder names that were changed in the above step

Open the solution and remove the invalid references pointing to the old ServiceModel and ServiceInterface projects, add references to renamed projects

Rebuild all






