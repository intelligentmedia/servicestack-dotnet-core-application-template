﻿using ServiceStack;

namespace AddedValue.SendGrid.ServiceModel
{
    [Route("/Order", Verbs = "POST")]
    public class Order : IReturn<OrderResponse>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class OrderResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
